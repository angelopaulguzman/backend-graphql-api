package com.graphql.guz.springbootgraphql.graphql;

import com.graphql.guz.springbootgraphql.documents.Article;
import com.graphql.guz.springbootgraphql.repositories.ArticleRepository;
import graphql.kickstart.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;

@Controller
@CrossOrigin("*")
public class ArticleQuery implements GraphQLQueryResolver {

    private ArticleRepository articleRepository;

    @Autowired
    public ArticleQuery(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public List<Article> articles() {
        return this.articleRepository.findAll();
    }


}
