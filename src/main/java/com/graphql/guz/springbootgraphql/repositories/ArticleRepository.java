package com.graphql.guz.springbootgraphql.repositories;

import com.graphql.guz.springbootgraphql.documents.Article;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ArticleRepository extends MongoRepository<Article, String> {
}
